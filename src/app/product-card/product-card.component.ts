import { Component, Input, OnInit } from '@angular/core'
import { Product } from '../shared/interfaces'
import { Router } from '@angular/router'
import { ProductService } from '../shared/product.service'

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() productCard: Product

  constructor(
    private router: Router,
    private productService: ProductService
  ) { }

  goToProductPage(id: string | undefined): void {
    if (id) {
      this.router.navigate(['product', id])
    }
  }

  ngOnInit(): void { }

  log(p: any) {
    // console.log('PRODUCT click: ',p)
    // console.log(this.productService.cart)
  }

  addToCart(product: Product) {
    this.productService.addProductToCart(product)
  }
}
