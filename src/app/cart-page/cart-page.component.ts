import { Component, OnDestroy, OnInit } from '@angular/core'
import { ProductService } from '../shared/product.service'
import { Order, Product } from '../shared/interfaces'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { OrderService } from '../shared/order.service'
import { Subscription } from 'rxjs'
import { Router } from '@angular/router'

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent implements OnInit, OnDestroy {

  cart: Product[] = []
  cartForm: FormGroup
  orderSub: Subscription

  constructor(
    private productService: ProductService,
    private orderService: OrderService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.cart = JSON.parse(localStorage.getItem('cart') as string)
    this.cartForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      phone: new FormControl(null, [Validators.required]),
      address: new FormControl(null, [Validators.required])
    })
  }

  cartClear() {
    this.productService.cartClear()
    this.cart = []
  }

  totalPrice() {
    return this.cart.reduce((t, i) => t + +i.price, 0)
  }

  removeFromCart(i: number) {
    this.productService.removeProductFromCart(i)
    this.cart = JSON.parse(localStorage.getItem('cart') as string)
  }

  onSubmit() {
    const order: Order = {
      name: this.cartForm.value.name,
      phone: this.cartForm.value.phone,
      address: this.cartForm.value.address,
      cart: this.cart
    }
    console.log(order)
    this.orderSub = this.orderService.createOrder(order).subscribe({
      next: () => {
        this.cartClear()
        this.cartForm.reset()
        this.router.navigate(['/'])
      },
      error: (err) => alert('Неведомая ошибка: ' + err),
      complete: () => alert('Заказ добавлен в базу данных!')
    })
  }

  ngOnDestroy(): void {
    if (this.orderSub) {
      this.orderSub.unsubscribe()
    }
  }


}
