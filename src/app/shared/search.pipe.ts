import { Pipe, PipeTransform } from '@angular/core'
import { Product } from './interfaces'


@Pipe({
  name: 'search'
})

export class SearchPipe implements PipeTransform {
  transform(products: Product[], searchValue = ''): Product[] {
    if (!searchValue.trim()) {
      return products
    }
    return products.filter((product: Product) => {
      return product.brand.toLowerCase().includes(searchValue.toLowerCase()) || product.title.toLowerCase().includes(searchValue.toLowerCase())
    })
  }

}
