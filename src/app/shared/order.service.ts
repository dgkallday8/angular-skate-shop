import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Order, Product } from './interfaces'
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private http: HttpClient
  ) { }


  createOrder(order: Order) {
    return this.http.post<Product>(`${environment.fbDataBaseUrl}/orders.json`, order)
  }


}
