export interface AdminUserLogin {
  email: string,
  password: string,
  returnSecureToken: boolean
}

export interface Product {
  id?: string
  title: string
  brand: string
  price: string
  productType: string
  description: string
  photo: string
}

export interface Order {
  id?: string
  name: string
  phone: string
  address: string,
  cart: Product[]
}
