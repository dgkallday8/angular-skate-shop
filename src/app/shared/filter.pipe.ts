import { Pipe, PipeTransform } from '@angular/core'
import { Product } from './interfaces'


@Pipe({
  name: 'filter'
})

export class FilterPipe implements PipeTransform {
  transform(products: Product[], filterValue: string): Product[] {
    if (filterValue === 'all') {
      return products
    }
    return products.filter((product: Product) => {
      return product.productType.toLowerCase() === filterValue.toLowerCase()
    })
  }

}
