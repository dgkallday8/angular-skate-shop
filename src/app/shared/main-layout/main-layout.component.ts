import { Component, OnInit } from '@angular/core'
import { AuthService } from '../auth.service'
import { Router } from '@angular/router'
import { ProductService } from '../product.service'

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router,
    private productService: ProductService
  ) { }

  ngOnInit(): void { }

  isAuth(): boolean {
    return this.auth.isAuth()
  }

  setType(type: string): void {
    this.productService.setType(type)
    this.router.navigate(['/'])
  }

  get filterType() {
    return this.productService.filterType
  }

  get cartCount() {
    return this.productService.cart.length
  }

}
