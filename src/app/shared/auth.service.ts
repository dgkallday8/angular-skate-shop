import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { AdminUserLogin } from './interfaces'
import { environment } from '../../environments/environment'
import { Observable, tap } from 'rxjs'

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(adminUser: AdminUserLogin): Observable<any> {
    return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, adminUser)
      .pipe(
        tap(this.setToken)
      )
  }

  setToken(response: any): void {
    if (response) {
      const expiryDate = new Date(new Date().getTime() +  +response.expiresIn * 1000)
      localStorage.setItem('fb-token-exp', expiryDate.toString())
      localStorage.setItem('fb-token', response.idToken)
    } else {
      localStorage.clear()
    }
  }

  get token(): string | null {
    const expiryDate = new Date(`${localStorage.getItem('fb-token-exp')}`)
    if (new Date > expiryDate) {
      this.logout()
      return null
    }
    return localStorage.getItem('fb-token')
  }

  logout(): void {
    this.setToken(null)
  }

  isAuth(): boolean {
    return !!this.token
  }

}
