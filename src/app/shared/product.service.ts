import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment'
import { map, Observable } from 'rxjs'
import { Product } from './interfaces'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  filterType = 'all'

  cart: Product[] = JSON.parse(`${localStorage.getItem('cart')}`) || []

  constructor(
    private http: HttpClient
  ) { }

  createProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(`${environment.fbDataBaseUrl}/products.json`, product)
  }

  getAllProducts(): Observable<any> {
    return this.http.get(`${environment.fbDataBaseUrl}/products.json`)
      .pipe(map( (res: any) => {
        if (res === null) {
          return []
        }
        return Object
          .keys(res)
          .map((key: string) => {
            return {
              ...res[key],
              id: key
            }
          })
      }))
  }

  getProductById(id: string): Observable<any> {
    return this.http.get(`${environment.fbDataBaseUrl}/products/${id}.json`)
      .pipe(
        map((res) => {
          return {
            ...res,
            id
          }
        })
      )
  }

  updateProduct(product: Product, id: string): Observable<any> {
    return this.http.patch(`${environment.fbDataBaseUrl}/products/${id}.json`, product)
  }

  removeProduct(id: string): Observable<Product> {
    return this.http.delete<Product>(`${environment.fbDataBaseUrl}/products/${id}.json`)
  }

  setType(type: string): void {
    this.filterType = type
  }

  addProductToCart(product: Product) {
    this.cart.push(product)
    localStorage.setItem('cart', JSON.stringify(this.cart))
  }

  removeProductFromCart(i: number) {
    this.cart.splice(i, 1)
    localStorage.setItem('cart', JSON.stringify(this.cart))
  }

  cartClear() {
    this.cart = []
    localStorage.clear()
  }

}
