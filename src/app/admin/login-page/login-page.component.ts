import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { AdminUserLogin } from '../../shared/interfaces'
import { AuthService } from '../../shared/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  form: FormGroup
  submitted = false

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
  }

  isAuth() {
    return this.authService.isAuth()
  }

  onSubmit() {
    if (this.form.invalid) {
      return
    }
    this.submitted = true
    const admin: AdminUserLogin = {
      email: this.form.value.email,
      password: this.form.value.password,
      returnSecureToken: true
    }
    this.authService.login(admin).subscribe({
      next: () => {
        this.form.reset()
        this.router.navigate(['/admin', 'dashboard'])
        this.submitted = false
      },
      error: () => {
        this.submitted = false
        alert('Ошибка, проверьте правильность введенных данных!')
      }
    })
  }

}
