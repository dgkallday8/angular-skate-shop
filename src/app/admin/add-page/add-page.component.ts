import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { ProductService } from '../../shared/product.service'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Product } from '../../shared/interfaces'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styleUrls: ['./add-page.component.scss']
})
export class AddPageComponent implements OnInit, OnDestroy {

  @ViewChild('fileInput') imgRef: ElementRef

  form: FormGroup
  submitted = false
  addSub: Subscription
  imageProduct = ''

  constructor(
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      brand: new FormControl(null, [Validators.required]),
      price: new FormControl(null, [Validators.required]),  // todo сделать свой валидатор
      productType: new FormControl(null, [Validators.required]),
      description: new FormControl(null, Validators.required),
    })
  }

  onSubmit(): void {
    this.submitted = true
    const product: Product = {
      title: this.form.value.title.trim(),
      price: this.form.value.price.trim(),
      photo: this.imageProduct.trim(),
      description: this.form.value.description.trim(),
      brand: this.form.value.brand.trim(),
      productType: this.form.value.productType.trim()
    }
    this.addSub = this.productService.createProduct(product).subscribe({
      next: () => {
        this.formClear()
        this.submitted = false
      },
      error: (err: Error) => {
        this.submitted = false
        alert('Произошла ошибка: ' + err.message)
      },
      complete: () => alert('Данные успешно добавлены!')
    })
  }

  selectFile(): void {
    this.imgRef.nativeElement.click()
  }

  loadFile(event: Event) {
    const target = event.target as HTMLInputElement
    if (!target.files) {
      return
    }
    const file: File = target.files[0]
    const reader: FileReader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      this.imageProduct = reader.result as string
    }
  }

  formClear() {
    this.form.reset()
    this.imageProduct = ''
  }

  ngOnDestroy() {
    console.log('[add-page] ngOnDestroy')
    if (this.addSub) {
      this.addSub.unsubscribe()
    }
  }

}

