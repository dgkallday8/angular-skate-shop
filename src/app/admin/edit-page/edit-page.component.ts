import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { ActivatedRoute, Params, Router } from '@angular/router'
import { Subscription, switchMap } from 'rxjs'
import { ProductService } from '../../shared/product.service'
import { Product } from '../../shared/interfaces'
import { FormControl, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss']
})
export class EditPageComponent implements OnInit, OnDestroy {

  @ViewChild('fileInput') imgRef: ElementRef

  product: Product
  imageProduct = ''
  form: FormGroup
  submitted = false
  editSub: Subscription
  id: string

  constructor(
    private activateRoute: ActivatedRoute,
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activateRoute.params.pipe(
      switchMap((params: Params) => {
        this.id = params['id']
        return this.productService.getProductById(params['id'])
      })
    ).subscribe(product => {
      this.product = product as Product
      this.imageProduct = this.product.photo
      this.form = new FormGroup({
        title: new FormControl(this.product.title, [Validators.required]),
        brand: new FormControl(this.product.brand, [Validators.required]),
        price: new FormControl(this.product.price, [Validators.required]),
        productType: new FormControl(this.product.productType, [Validators.required]),
        description: new FormControl(this.product.description, [Validators.required]),
      })
    })
  }

  loadFile(event: Event) {
    const target = event.target as HTMLInputElement
    if (!target.files) {
      return
    }
    const file: File = target.files[0]
    const reader: FileReader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      this.imageProduct = reader.result as string
    }
  }

  selectFile(): void {
    this.imgRef.nativeElement.click()
  }

  formClear(): void {
    this.form.reset()
    this.imageProduct = ''
  }

  onSubmit(): void {
    this.submitted = true
    const product = {
      title: this.form.value.title.trim(),
      price: this.form.value.price.trim(),
      photo: this.imageProduct.trim(),
      description: this.form.value.description.trim(),
      brand: this.form.value.brand.trim(),
      productType: this.form.value.productType.trim()
    }

    this.editSub = this.productService.updateProduct(product, this.id).subscribe({
      next: () => {
        this.submitted = false
        this.formClear()
        this.router.navigate(['/admin', 'dashboard'])
      },
      error: (err: Error) => alert('Ошибка изменения товара: ' + err.message),
      complete: () => alert('Данные о товаре успешно изменены!')
    })
  }

  ngOnDestroy(): void {
    console.log('[edit-page] ngOnDestroy')
    if (this.editSub){
      this.editSub.unsubscribe()
    }
  }



}
