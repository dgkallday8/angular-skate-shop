import { Component, OnDestroy, OnInit } from '@angular/core'
import { ProductService } from '../../shared/product.service'
import { Product } from '../../shared/interfaces'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {

  allProducts: Product[] = []
  getAllProductsSubscription: Subscription
  removeSubscription: Subscription


  constructor(
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.getAllProductsSubscription = this.productService.getAllProducts().subscribe({
      next: products => {
        this.allProducts = products
      },
      error: err => alert(err)
    })
  }




  removeProduct(id: string | undefined): void {
    if (id) {
      this.removeSubscription = this.productService.removeProduct(id).subscribe({
        next: () => {
          this.allProducts = this.allProducts.filter(product => product.id !== id)
          alert('Товар спешно удалён!')
        },
        error: (error: Error) => alert('Произошла ошибка удаления: ' + error.message)
      })
    }

  }




  ngOnDestroy(): void {
    if (this.getAllProductsSubscription) {
      this.getAllProductsSubscription.unsubscribe()
    }
    if (this.removeSubscription) {
      this.removeSubscription.unsubscribe()
    }
  }
}
