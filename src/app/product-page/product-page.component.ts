import { Component, OnInit } from '@angular/core'
import { ProductService } from '../shared/product.service'
import { Product } from '../shared/interfaces'
import { ActivatedRoute, Params, Router } from '@angular/router'
import { switchMap } from 'rxjs'

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  product: Product

  constructor(
    private productService: ProductService,
    private activateRoute: ActivatedRoute,
    private router: Router

) { }

  ngOnInit(): void {
    this.activateRoute.params.pipe(
      switchMap((params: Params) => {
        return this.productService.getProductById(params['id'])
      })
    ).subscribe({
      next: (product) => {
        this.product = product as Product
      }
    })
  }

  toMainPage() {
    this.router.navigate([''])
  }
}
