import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { AuthInterceptor } from './shared/auth.interceptor'
import { SearchPipe } from './shared/search.pipe'
import { FilterPipe } from './shared/filter.pipe'

import { AppComponent } from './app.component'
import { MainLayoutComponent } from './shared/main-layout/main-layout.component'
import { MainPageComponent } from './main-page/main-page.component'
import { CartPageComponent } from './cart-page/cart-page.component'
import { ProductCardComponent } from './product-card/product-card.component'
import { ProductPageComponent } from './product-page/product-page.component'

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    MainPageComponent,
    CartPageComponent,
    ProductCardComponent,
    ProductPageComponent,
    SearchPipe,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: AuthInterceptor
    }
  ]
})
export class AppModule { }


